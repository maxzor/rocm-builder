#!/bin/bash
echo "not meant to be currently run, this is a random bunch of stuff"
exit


# -------------------------------- AMD sources --------------------------------
# Currently this simple script prints the three known github organisations
# that hold ROCm sources, and the names of the repositories that they expose.
for org in radeonopencompute rocm-developer-tools rocmsoftwareplatform; do
	echo $org
	curl -s https://api.github.com/orgs/$org/repos \
	| jq --raw-output '.[] | .name' \
	| tr '[:upper:]' '[:lower:]' \
	| sort
	#| jq --raw-output '.[] | .svn_url' \
	#| tr '[:upper:]' '[:lower:]' \
	#| sed -n "s/https:\/\/github.com\/$org\///p" \
	echo ""
done

# ...with urls
for org in radeonopencompute rocm-developer-tools rocmsoftwareplatform; do
	echo $org
	curl -s https://api.github.com/orgs/$org/repos \
	| jq --raw-output '.[] | [.name, .description]|join(": ")' \
	| tr '[:upper:]' '[:lower:]' \
	| sort
	echo ""
done
	#| jq --raw-output '.[] | [.name, .svn_url]|join(": ")' \



# Clone all the repositories of one ROCm github organisation
curl https://api.github.com/orgs/rocm-developer-tools/repos \
	| jq '.[] | .clone_url' | xargs -Iyo git clone yo

# Make a json simplified repo list for one org
curl https://api.github.com/orgs/rocm-developer-tools/repos \
	| jq '.[] | {description:.description, url:.svn_url}' > repos.json

# ------------------------------ Debian packaging -----------------------------

#!/bin/bash
list_repo_urls() {
	#curl "https://salsa.debian.org/api/v4/groups/rocm-team/projects" \
	curl "https://gitlab.com/api/v4/groups/14572752/projects" \
	| jq -r '.[] | .http_url_to_repo'
}

while read -r repo_url; do
	repo_name=$(echo $repo_url | sed -n 's%https://gitlab.com/rocm_salsa/cubana/\(.*\).git%\1%p')
	mkdir $repo_name
	git clone $repo_url $repo_name/$repo_name || exit
done < <(list_repo_urls)



# Print the upstream links for the salsa repos
for f in *; do
    if [ -d "$f" ]; then
        echo "$f: $(grep -s "Homepage" $f/$f/debian/control)"
    fi
done

#corectrl: Homepage: https://gitlab.com/corectrl/corectrl
#reference-docs: 
#rocm-cmake: Homepage: https://github.com/RadeonOpenCompute/rocm-cmake
#rocm-compilersupport: Homepage: https://github.com/RadeonOpenCompute/ROCm-CompilerSupport
#rocm-device-libs: Homepage: https://github.com/RadeonOpenCompute/ROCm-Device-Libs/
#rocm-hip: 
#rocminfo: Homepage: https://github.com/RadeonOpenCompute/rocminfo
#rocm-llvm: Homepage: https://github.com/RadeonOpenCompute/llvm-project
#rocm-smi-lib: Homepage: https://github.com/RadeonOpenCompute/rocm_smi_lib
#rocr-runtime: Homepage: https://github.com/RadeonOpenCompute/ROCR-Runtime/
#roc-smi: Homepage: https://github.com/RadeonOpenCompute/ROC-smi
# roct-thunk-interface: Homepage: https://github.com/RadeonOpenCompute/ROCT-Thunk-Interface



# --------------------------------- ROC Thunk ---------------------------------
# /!\ IN AN EMPTY DIRECTORY
sudo rm -r ./*
git clone https://salsa.debian.org/rocm-team/roct-thunk-interface
cd roct-thunk-interface/
# Why do I need this?
git checkout upstream && git checkout pristine-tar && git checkout master
# Bump version
echo | gbp import-orig --uscan 
# Update changelog
gbp dch

sed -i 's/libhsakmt.so/libhsakmt.a/' debian/libhsakmt-dev.install
sed -i 's/libhsakmt.so.1/libhsakmt.a/' debian/libhsakmt1.symbols
sed -i 's/libhsakmt.so.*/libhsakmt.a/' debian/libhsakmt1.install
git commit -am "Build static: install static"
gbp buildpackage




sudo rm -r ./*
git clone https://deb:glpat-Ebi6hMkrh1NLtHE_Qpz7@gitlab.com/salsa_cubana/roct-thunk-interface
cd rocr-runtime
git checkout upstream && git checkout pristine-tar && git checkout master
gbp buildpackage


sudo rm -r ./*
git clone https://deb:glpat-Ebi6hMkrh1NLtHE_Qpz7@gitlab.com/salsa_cubana/rocm-cmake
cd rocm-cmake
git checkout upstream && git checkout pristine-tar && git checkout master
gbp buildpackage

sudo rm -r ./*
git clone https://deb:glpat-Ebi6hMkrh1NLtHE_Qpz7@gitlab.com/salsa_cubana/rocm-device-libs
cd rocm-device-libs
git checkout upstream && git checkout pristine-tar && git checkout master
gbp buildpackage

sudo rm -r ./*
git clone https://deb:glpat-Ebi6hMkrh1NLtHE_Qpz7@gitlab.com/salsa_cubana/rocm-smi-libs
cd rocm-smi-libs
git checkout upstream && git checkout pristine-tar && git checkout master
gbp buildpackage


# -------------------------------- ROC Runtime --------------------------------
sudo rm -r ./*
git clone https://salsa.debian.org/maxzor/rocr-runtime
cd rocr-runtime
git checkout upstream && git checkout pristine-tar && git checkout master
gbp buildpackage


# --------------------------------- ROC Comgr ---------------------------------
wget https://github.com/RadeonOpenCompute/ROCm-CompilerSupport/archive/refs/heads/amd-stg-open.zip
unzip amd-stg-open.zip
tar czf rocm-comgr_4.5.2.1.tar.gz ROCm-CompilerSupport-amd-stg-open
mv rocm-comgr_4.5.2.1.tar.gz ..
sudo rm -r ./*
mv ../rocm-comgr_4.5.2.1.tar.gz .
git clone https://salsa.debian.org/maxzor/rocm-compilersupport rocm-compilersupport
cd rocm-compilersupport
git checkout upstream && git checkout pristine-tar && git checkout master
echo | gbp import-orig ../rocm-comgr_4.5.2.1.tar.gz
gbp buildpackage

# gbp pq workflow
# Add a local patch
gbp pq import
sed -i 's/libhsakmt.so/libhsakmt.a/' debian/libhsakmt-dev.install
git commit -am "Build static: install static."
gbp pq export
git add --all
git commit -m "Build static: install static."

# ---------------------------------- Big HIP ----------------------------------
cd /debamd/rocm-hipamd
sudo rm -r /debamd/rocm-hipamd/rocm-hipamd
git clone https://salsa.debian.org/maxzor/rocm-hipamd
wget -O rocm-hipamd_4.5.2.orig.tar.gz https://github.com/ROCm-Developer-Tools/hipamd/archive/refs/tags/rocm-4.5.2.tar.gz
wget -O rocm-hipamd_4.5.2.orig-hip.tar.gz https://github.com/ROCm-Developer-Tools/HIP/archive/refs/tags/rocm-4.5.2.tar.gz
wget -O rocm-hipamd_4.5.2.orig-clr.tar.gz https://github.com/ROCm-Developer-Tools/ROCclr/archive/refs/tags/rocm-4.5.2.tar.gz
wget -O rocm-hipamd_4.5.2.orig-opencl.tar.gz https://github.com/RadeonOpenCompute/ROCm-OpenCL-Runtime/archive/refs/tags/rocm-4.5.2.tar.gz

cd rocm-hipamd
tar --strip 1 -zxf ../rocm-hipamd_4.5.2.orig.tar.gz
mkdir -p hip clr opencl
tar --strip 1 -C hip -zxf ../rocm-hipamd_4.5.2.orig-hip.tar.gz
tar --strip 1 -C clr -zxf ../rocm-hipamd_4.5.2.orig-clr.tar.gz
tar --strip 1 -C opencl -zxf ../rocm-hipamd_4.5.2.orig-opencl.tar.gz


git remote add salsam https://salsa.debian.org/maxzor/rocm-compilersupport
git checkout upstream && git checkout pristine-tar && git checkout master
git push --force salsam master
git push --force salsam upstream
git push --force salsam pristine-tar

#rocRAND
wget -O rocblas_4.5.2.tar.gz https://github.com/rocmsoftwareplatform/rocrand/archive/refs/tags/rocm-4.5.2.tar.gz



# Rocblas

wget -O rocblas_4.5.2.tar.gz https://github.com/rocmsoftwareplatform/rocblas/archive/refs/tags/rocm-4.5.2.tar.gz
wget -O rocblas_4.5.2.orig-tensile.tar.gz https://github.com/rocmsoftwareplatform/tensile/archive/refs/tags/rocm-4.5.2.tar.gz
...
gbp import-orig --pristine-tar --component=tensile ../rocblas_4.5.2.tar.gz

# rocm-opencl-runtime
gbp import-orig --pristine-tar --component=clr --uscan
# rocclr
gbp import-orig --pristine-tar --component=opencl --uscan

# Install the packages in order

sudo dpkg -i rocm-cmake_4.5.2-1~exp1_all.deb \
			 rocm-device-libs_4.5.2-1~exp1_amd64.deb \
			 rocminfo_4.5.2-1~exp1_amd64.deb \
			 libhsakmt1_4.5.2+dfsg-1_amd64.deb \
			 libhsakmt-dev_4.5.2+dfsg-1_amd64.deb \
			 libhsa-runtime64-1_4.5.2-1_amd64.deb \
			 libhsa-runtime-dev_4.5.2-1_amd64.deb \
			 liboam1_4.5.2-1~exp1_amd64.deb \
			 liboam-dev_4.5.2-1~exp1_all.deb \
			 librocm-smi64-1_4.5.2-1~exp1_amd64.deb \
			 librocm-smi-dev_4.5.2-1~exp1_amd64.deb \
			 librocm-smi-tools_4.5.2-1~exp1_amd64.deb \
			 rocm-comgr_4.5.2+git-20220125-a75326c7-1~exp1_amd64.deb \
			 libhiprtc-builtins4_4.5.2-1~exp1_amd64.deb \
			 libamdhip64-4_4.5.2-1~exp1_amd64.deb \
			 libamdhip64-dev_4.5.2-1~exp1_all.deb \
			 librocrand1_4.5.2-1~exp1_amd64.deb \
			 librocrand1-dev_4.5.2-1~exp1_amd64.deb \
			 libhiprand1_4.5.2-1~exp1_amd64.deb \
			 libhiprand1-dev_4.5.2-1~exp1_amd64.deb \
			 librocblas0_4.5.2-1~exp1_amd64.deb \
			 librocblas0-dev_4.5.2-1~exp1_amd64.deb

# Install the hip debug packages in order

sudo dpkg -i rocm-device-libs_4.5.2-1~exp1_amd64.deb \
			 rocminfo_4.5.2-1~exp1_amd64.deb \
			 libhsakmt1_4.5.2+dfsg-1_amd64.deb \
			 libhsakmt1-dbgsym_4.5.2+dfsg-1_amd64.deb \
			 libhsakmt-dev_4.5.2+dfsg-1_amd64.deb \
			 libhsa-runtime64-1_4.5.2-1_amd64.deb \
			 libhsa-runtime64-1-dbgsym_4.5.2-1_amd64.deb \
			 libhsa-runtime-dev_4.5.2-1_amd64.deb \
			 liboam1_4.5.2-1~exp1_amd64.deb \
			 liboam-dev_4.5.2-1~exp1_all.deb \
			 librocm-smi64-1_4.5.2-1~exp1_amd64.deb \
			 librocm-smi-dev_4.5.2-1~exp1_amd64.deb \
			 librocm-smi-tools_4.5.2-1~exp1_amd64.deb \
			 rocm-comgr_4.5.2+git-20220125-a75326c7-1~exp1_amd64.deb \
			 rocm-comgr-dbgsym_4.5.2+git-20220125-a75326c7-1~exp1_amd64.deb \
			 libhiprtc-builtins4_4.5.2-1~exp1_amd64.deb \
			 libhiprtc-builtins4-dbgsym_4.5.2-1~exp1_amd64.deb \
			 libamdhip64-4_4.5.2-1~exp1_amd64.deb \
			 libamdhip64-4-dbgsym_4.5.2-1~exp1_amd64.deb \
			 libamdhip64-dev_4.5.2-1~exp1_all.deb

# Uninstall
sudo dpkg -r rocm-device-libs \
			 rocminfo \
			 libhsakmt1-dbgsym \
			 libhsakmt1 \
			 libhsakmt-dev \
			 libhsa-runtime64-1-dbgsym \
			 libhsa-runtime64-1 \
			 libhsa-runtime64-dev \
			 liboam1 \
			 liboam-dev \
			 librocm-smi64-1 \
			 librocm-smi-dev \
			 librocm-smi-tools \
			 rocm-comgr-dbgsym \
			 rocm-comgr \
			 libhiprtc-builtins4-dbgsym \
			 libhiprtc-builtins4 \
			 libamdhip64-4-dbgsym \
			 libamdhip64-4 \
			 libamdhip64-dev


# Git-repo superproject launch
repo init -u https://salsa.debian.org/rocm-team/community/team-project
repo sync
# Bring the branches locally
repo forall -c 'git checkout pristine-tar && git checkout upstream && git checkout master'
# Check that all is nice and beautiful
repo status
repo info
# Start doing work
repo forall -c "sed -i 's/team+rocm-team@tracker.debian.org/debian-ai@lists.debian.org/' debian/control"
repo forall -c "git commit -am 'Update maintainer address to debian-ai@l.d.o.' && git push rocm-team master"

repo forall -c "sed -i 's%/usr/amdgcn/bitcode%/usr/share/amdgcn/bitcode%' debian/rules"
repo forall -c "git commit -am 'Trickle up bitcode install in /usr/share/amdgcn instead of /usr/amdgcn' && git push rocm-team master"

# Import orig reset, danger levels
rm ../* || rm -r * && sudo rm -r ./.* && cp -r ../debian/ .

# Building sub-ranges
./3_build_packages_in_docker.sh -f rocr-runtime -t rocm-hipamd
man rocm-smi-lib/rocm-smi-lib/debian/roc-smi.1

# Update all packages to ROCm 5.x
# from builder/salsa :
repo forall -c "echo | gbp import-orig --uscan"
repo forall -c "gbp dch && git commit -am 'Update d/changelog to ROCm 5.x'"