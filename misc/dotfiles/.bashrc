DEBEMAIL="maxzor@maxzor.eu"
DEBFULLNAME="Maxime Chambonnet"
export DEBEMAIL DEBFULLNAME

alias dquilt="quilt --quiltrc=${HOME}/.quiltrc-dpkg"
complete -F _quilt_completion $_quilt_complete_opt dquilt
