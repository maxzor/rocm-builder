#!/bin/bash
# -------------------------- Initial source download --------------------------
# Download salsa-Debian-package-ROCm source repositories

# Help/error message
usage() { echo -e \
"Usage: $0 [-r --repo] [-h --help]\n"\
"Download ROCm Debian package sources from salsa, either with curl and
jq (default), or with git-repo (package in Debian contrib):
    './1_install_salsa_repos.sh -r'."
    1>&2; exit 1; }

download_repo_style() {
    printf '%.s-' {1..80} && printf '\n'
    echo Downloading sources with git-repo
    printf '%.s-' {1..80} && printf '\n'
    mkdir salsa || exit
    cd salsa
    # Git-repo superproject launch
    repo init -u https://salsa.debian.org/rocm-team/community/team-project
    repo sync
    # Bring the branches locally
    repo forall -c 'git checkout pristine-tar && git checkout upstream && git checkout master'
}

list_repo_urls() {
    # This is currently my own repos, TODO rocm-team
    curl "https://salsa.debian.org/api/v4/users/11741/projects" \
    | jq -r '.[] | .http_url_to_repo'
}

download_jq_style() {
    printf '%.s-' {1..80} && printf '\n'
    echo Downloading sources with curl and jq
    printf '%.s-' {1..80} && printf '\n'
    mkdir salsa || exit
    while read -r repo_url; do
        repo_name=$(echo $repo_url | \
            sed -n 's%https://salsa.debian.org/maxzor/\(.*\).git%\1%p')
        if [[ -d "$repo_name" ]]; then
            echo "It appears that $repo_name is already downloaded, skipping."
            continue
        fi
        # source directory is nested to allow room for the debian packages, which
        # land, after building, one layer above source, in ./repo_name or "src/.."
        git clone $repo_url salsa/$repo_name/$repo_name || exit
    done < <(list_repo_urls)
}

# Argument parser with posix getopts (non-free...)
# dual-state with default, long option mode with getopts
die() { echo "$*" >&2; exit 2; }  # complain to STDERR and exit with error
needs_arg() { if [ -z "$OPTARG" ]; then die "No arg for --$OPT option"; fi; }

if [[ $1 == "" ]]; then
    download_jq_style
    exit;
else
    while getopts "rh-:" OPT; do
        if [ "$OPT" = "-" ]; then   # long option: reformulate OPT and OPTARG
            OPT="${OPTARG%%=*}"       # extract long option name
            OPTARG="${OPTARG#$OPT}"   # extract long option argument (may be empty)
            OPTARG="${OPTARG#=}"      # if long option argument, remove assigning `=`
        fi
        case "${OPT}" in
            r | repo )	download_repo_style;;
            h | help )	usage;;
            *)			usage;;
        esac
    done
    shift $((OPTIND-1))
fi